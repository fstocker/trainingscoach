package com.fritz;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.app.Activity;
import android.os.Handler;
import android.view.Menu;
import android.view.View;
import android.widget.*;

import java.text.DecimalFormat;

public class MainActivity extends Activity {

    private LocationListener providerListener = null;
    private LocationManager locationManager = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        final Handler h = new Handler();
        h.postDelayed(new Runnable()
        {
            private long time = 0;

            @Override
            public void run()
            {
                // do stuff then
                // can call h again after work!
                computeAndDisplaySpeed();
                computeAndDisplayDistance();
                h.postDelayed(this, 2000);
            }
        }, 2000); // 1 second delay (takes millis)
        initProvider();
    }

    private void initProvider(){
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        providerListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                TextView myTextView = (TextView)
                        findViewById(R.id.textView);
                myTextView.setText("onLocationChanged");
            }
            public void onStatusChanged(String provider, int status, Bundle extras) {
                TextView myTextView = (TextView)
                        findViewById(R.id.textView);
                myTextView.setText("onStatusChanged");
                makeUseOfStatus(status);
            }

            public void onProviderEnabled(String provider) {
                TextView myTextView = (TextView)
                        findViewById(R.id.textView);
                myTextView.setText("onProviderEnabled");
            }

            public void onProviderDisabled(String provider) {
                TextView myTextView = (TextView)
                        findViewById(R.id.textView);
                myTextView.setText("onProviderDisabled");
            }
        };
        // Register the listener with the Location Manager to receive location updates
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1L, 0f, providerListener);

    }

    private void makeUseOfStatus(int status) {
        if( status == LocationProvider.AVAILABLE){
            /*locationListener = new LocationListener() {
                public void onLocationChanged(Location location) {
                    // Called when a new location is found by the network location provider.
                    makeUseOfNewLocation(location);
                }
                public void onStatusChanged(String provider, int status, Bundle extras) {}

                public void onProviderEnabled(String provider) {}

                public void onProviderDisabled(String provider) {}
            };
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0L, 5.0f, locationListener);*/
            TextView myTextView = (TextView)
                    findViewById(R.id.textView);
            myTextView.setText("Signal valid");

            Button startButton = (Button)
                    findViewById(R.id.button);
            startButton.setEnabled(true);

            locationListener = null;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    private double longitude = 0.0;
    private double latitude = 0.0;
    private double lon = 0.0;
    private double lat = 0.0;
    private double distance = 0.0;
    long startzeitBeginn = 0;
    long startzeitLap = 0;
    double currSpeed = 0.0;
    double lastLat = 0;
    double speedGes = 0.0;
    int speedCounter = 0;
    int computeBeforeStart = 0;

    private void makeUseOfNewLocation(Location location) {
        if( computeBeforeStart++ < 5 )
            return;
        computeBeforeStart = 10;

        TextView myTextView = (TextView)
                findViewById(R.id.textView);
        myTextView.setText("Genauigkeit: " + location.getAccuracy());



        long timeDiffLap = 0;
        if( this.setzeAuf0 == true){
            this.setzeAuf0 = false;
            this.lon = 0.0;
            this.lat = 0.0;
            this.startzeitBeginn = 0;
            this.startzeitLap = 0;
        }
        if( startzeitBeginn == 0)
            startzeitBeginn = System.currentTimeMillis();

        startzeitLap = System.currentTimeMillis();
        longitude = location.getLongitude();
        if(lon == 0.0)
            lon = longitude;
        latitude = location.getLatitude();
        if( lat == 0.0)
            lat = latitude;


        if( lat != 0.0)
        {
            double _d2r = (Math.PI / 180D);
            double _eQuatorialEarthRadius = 6378.1370D;
            double dlong = (this.longitude - this.lon) * _d2r;
            double dlat = (this.latitude - this.lat) * _d2r;
            double a = Math.pow(Math.sin(dlat / 2D), 2D) + Math.cos(lat * _d2r) * Math.cos(this.latitude * _d2r) * Math.pow(Math.sin(dlong / 2D), 2D);
            double c = 2D * Math.atan2(Math.sqrt(a), Math.sqrt(1D - a));
            distance += _eQuatorialEarthRadius * c;

        }

//Und die alte Position wieder merken
        /*myTextView.setText("Startpos = \nLON: " + this.lon + "\nLAT: " + this.lat +
                "\n\nPosition = \nLON: " + this.longitude + "\nLAT: " + this.latitude + "\nDIS: " + distance +
                "\n\nSpeedGesamt = " + speedGes +
                "\n\nAktuelle Speed: " + location.getSpeed() * 3.6);*/

        currSpeed = location.getSpeed() * 3.6;

        this.lon = longitude;
        this.lat = latitude;


    }

    DecimalFormat f = new DecimalFormat("#0.00");
    private void computeAndDisplayDistance(){
        EditText myEditText2 = (EditText)
                findViewById(R.id.editText2);

        myEditText2.setText("" + f.format(distance));
    }
    private void computeAndDisplaySpeed(){
        if(this.latitude == 0.0)
            return;

        EditText myEditText = (EditText)
                findViewById(R.id.editText);

        TextView myTextView4 = (TextView)
                findViewById(R.id.textView4);


        long timeDiffStart = 0;
        if(startzeitBeginn > 0)
            timeDiffStart = System.currentTimeMillis()-startzeitBeginn;

        if( timeDiffStart > 0 )
            speedGes = (distance / timeDiffStart) * 1000 * 3600;




        if( lastLat == this.latitude  ){
            if( speedCounter++ < 10 )
                myEditText.setText(f.format(currSpeed) + "(e" + speedCounter + ") / Ges: "+f.format(speedGes));
            else{
                myEditText.setText("0.0 / Ges: "+f.format(speedGes));
                myTextView4.setBackgroundResource(android.R.color.holo_blue_light);
            }
            return;
        }

        speedCounter = 0;
        lastLat = this.latitude;
        if(currSpeed < 2.5)
            myTextView4.setBackgroundResource(android.R.color.holo_blue_bright);
        else if(currSpeed > 4)
            myTextView4.setBackgroundResource(android.R.color.holo_red_light);
        else
            myTextView4.setBackgroundResource(android.R.color.holo_green_light);

        myEditText.setText(""+f.format(currSpeed) + " / Ges: "+f.format(speedGes));
    }

    private boolean setzeAuf0 = true;
    public void onClick2(View v) {
//Jetzt wird der Startupunkt auf 0 gesetzt und ein neuer Messvorgang begonnen

        initProvider();
    }
    private LocationListener locationListener = null;

        public void onClick(View v) {

// Acquire a reference to the system Location Manager
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

// Define a listener that responds to location updates
            if( locationListener == null ){
        locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                // Called when a new location is found by the network location provider.
                makeUseOfNewLocation(location);
            }
            public void onStatusChanged(String provider, int status, Bundle extras) {}

            public void onProviderEnabled(String provider) {}

            public void onProviderDisabled(String provider) {}
        };
            }

        // Register the listener with the Location Manager to receive location updates
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0L, 5.0f, locationListener);




            TextView myTextView = (TextView)
                findViewById(R.id.textView);
        myTextView.setText("Gestartet");
    }
}
